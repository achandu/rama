#Rama app

This README documents whatever steps are necessary to get your application up and running for rama app.

#### What does the app do?
Rama app tries to create an emergency alarm for people in distress and trouble. We aim to provide a way so that people can call for help and this app would send a "HELP" call to your emergency contacts.

## What does it use ?
1. Android SDK
1. Robolectric (For unit testing)
1. Jacoco reports (For code coverage)
1. SDK Manager [SDK Manager plugin](https://github.com/JakeWharton/sdk-manager-plugin)


## How do I get set up? ###
1. Clone the repository [Github repo](https://github.com/rchandu/rama.git)
1. Dependencies
    1. Java (JDK)
    1. Gradle (Optional - You can setup using Android studio)
1. Go to terminal (make sure you can run gradle from your terminal)
    1. Go to "[repo_installed_directory]/android" directory
    1. gradle clean assemble
    1. Say Thanks to Jake Wharton for SDK Manager :D
1. Available tasks:
    1. gradle clean              (Cleans the build directory)
    1. gradle test               (Run tests)
    1. gradle jacocoTestReport   (Run code coverage)

## What was shipped recently?
#### version 0.3.2 : 2015-01-25
1. Sent Google-maps-openable link along with HELP message

#### version 0.3.1 : 2014-11-05
1. Renamed DataParser to ProtocolHandler
1. Added ProtocolHandler functions
  1. generateHelperStart()
  1. generateHelperStop()
  1. generateLocationReporter(Location)
1. ProtocolHandler tests

( For complete release notes, refer to changelog.md)

## How to contribute
1. Create a branch (Don't push changes on master.)
1. Write your Code and tests (Without tests, code won't be pulled into the master)
1. Run tests and see if they're green.
1. Rebase with master and resolve conflicts
1. Push your commits and create pull request to master

#### Who do I talk to? ###
* Rama Chandu [email me!](rama.chandu57@gmail.com)
