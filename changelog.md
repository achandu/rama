# Changelog

###LATEST RELEASE 
-----------------------
##### version 0.3.2 : 2015-01-25
1. Sent Google-maps-openable link along with HELP message

###OLDER VERSIONS
-----------------------
##### version 0.3.1 : 2014-11-05
1. Renamed DataParser to ProtocolHandler
1. Added ProtocolHandler functions
  1. generateHelperStart()
  1. generateHelperStop()
  1. generateLocationReporter(Location)
1. ProtocolHandler tests

##### version 0.3.0 : 2014-11-04
1. Fixate messaging format
2. DataParser - isHelperStart(), isHelperStop(), isLocationTracker()
3. Data Parser tests

##### version 0.2.3
1. Send location periodically until the user says he's safe.
1. Send current location co-ordinates also.

##### version 0.2.2 : 2014-10-20
1. Get current location.
2. Update UI by checking if app is calling "help".
    1. Update help button text based based on location being reported.
    2. Update help button color based based on location being reported.

##### version 0.2.1 : 2014-08-31
1. UI cleanup
2. Settings UI and preferences

##### version 0.2.0 : 2014-08-23
1. Sends Help message to emergency contact.
2. Emergency contact populate through preferences.
3. Tests up and running.


### Future releases
-----------------------
#### version 0.3.5: 2015-02-01
1. Add Google analytics.
2. Select emergency person through contacts.
3. Show list of messages sent through the app for sender.

##### version 0.4.0 : 2015-02-08
1. Listen to incoming messages
1. Parse received messages
1. On "help_start", set "location_recorder" as true
1. On "help_stop", set "location_recorder" as false

##### version 0.4.1: 2015-02-15
1. Show list of messages recieved by reciever from this app.
2. Show list of messages recieved categorized by sender.