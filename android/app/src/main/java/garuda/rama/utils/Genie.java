package garuda.rama.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Gandalf created this. This does magic and helps in your often-used functions.
 * Created by rchandu on 8/15/14.
 */
public class Genie {
    private static Context appContext;
    private static String helpPhoneNumber;
    public static final String PREFS_NAME = "RamaRunningPrefs";

    private static <T> void checkAndThrowNullPointerException(T toCheckObject) {
        if (toCheckObject == null) {
            throw new NullPointerException();
        }
    }

    public static void reset() {
        helpPhoneNumber = null;
        appContext = null;
    }

    // Alert #######################################################################################
    public static void alert(String displayMessage, Context currentContext) throws NullPointerException {
        Toast.makeText(currentContext, displayMessage, Toast.LENGTH_SHORT).show();
    }

    public static void alert(String displayMessage) throws NullPointerException {
        checkAndThrowNullPointerException(appContext);
        Toast.makeText(appContext, displayMessage, Toast.LENGTH_LONG).show();
    }

    // Context #####################################################################################
    public static void setAppContext(Context inAppContext) throws NullPointerException {
        appContext = inAppContext;
    }

    public static Context getAppContext() throws NullPointerException {
        checkAndThrowNullPointerException(appContext);
        return appContext;
    }

    // PhoneNumber #################################################################################
    public static void setPhoneNumber(String phoneNumberToSet, Context inContext) {
        if (isValidPhoneNumber(phoneNumberToSet)) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(inContext);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(StringConstants.HELP_NUMBER_PREF_KEY, phoneNumberToSet);
            editor.commit();
        }
    }

    public static String getPhoneNumber(Context inContext) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(inContext);
        return prefs.getString(StringConstants.HELP_NUMBER_PREF_KEY, "");
    }

    public static boolean isValidPhoneNumber(String toCheckPhoneNumber) {
        return true;
    }

    public static void sendSMS(String toSendMessage, String toSendPhoneNumber) {
        SmsManager smsManager = SmsManager.getDefault();
        ArrayList<String> parts = smsManager.divideMessage(toSendMessage);
        smsManager.sendMultipartTextMessage(toSendPhoneNumber, null, parts, null, null);
    }


    // isLocationReportingEnabled ##################################################################
    public static void setLocationReportingEnabled(boolean isLocationReportingEnabled, Context inContext) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(inContext);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(StringConstants.LOC_REPORTING_PREF_KEY, isLocationReportingEnabled);
        editor.commit();
    }

    public static boolean isLocationReportingEnabled(Context inContext) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(inContext);
        return prefs.getBoolean(StringConstants.LOC_REPORTING_PREF_KEY, true);
    }


    // isRamaCallingHelp ###########################################################################
    public static void setRamaCallingHelp(boolean isCallingHelp, Context inContext) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(inContext);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(StringConstants.RAMA_RUNNING_PREF_KEY, isCallingHelp);
        editor.commit();
    }

    public static boolean isRamaCallingHelp(Context inContext) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(inContext);
        return prefs.getBoolean(StringConstants.RAMA_RUNNING_PREF_KEY, false);
    }
}
