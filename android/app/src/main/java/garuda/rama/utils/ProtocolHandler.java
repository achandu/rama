package garuda.rama.utils;

import android.location.Location;

/**
 * Created by rchandu on 11/4/14.
 */
public class ProtocolHandler {
    private static final String helperStartPrefix = "rh:start";
    private static final String helperStopPrefix = "rh:stop";
    private static final String locationReporterPrefix = "rloc:";

    public static boolean isHelperStart(String toCheckData) {
        return toCheckData.startsWith(helperStartPrefix);
    }

    public static boolean isHelperStop(String toCheckData) {
        return toCheckData.startsWith(helperStopPrefix);
    }

    public static boolean isLocationReporter(String toCheckData) {
        final String locationReporterPattern = locationReporterPrefix + "[-+]?[\\d.]*,[-+]?[\\d.]*";
        return toCheckData.matches(locationReporterPattern);
    }

    public static String generateHelperStart() {
        return generateHelperStart("");
    }

    public static String generateHelperStart(String toAppendData) {
        String generatedHelperStart = helperStartPrefix + toAppendData;
        return generatedHelperStart;
    }

    public static String generateHelperStop() {
        return generateHelperStop("");
    }

    public static String generateHelperStop(String toAppendData) {
        String generatedHelperStopString = helperStopPrefix + toAppendData;
        return generatedHelperStopString;
    }

    public static String generateLocationReporter(Location targetLocation) {
        String generatedHelperStart = locationReporterPrefix
                + targetLocation.getLatitude() + ","
                + targetLocation.getLongitude();
        return generatedHelperStart;
    }
}
