package garuda.rama.utils;

import android.location.LocationManager;

/**
 * Created by rchandu on 8/10/14.
 */
public class StringConstants {
    // Display messages ############################################################################
    public static final String HELPME = "HELP!!!";
    public static final String IAMSAFE = "I'm safe...";

    public static final String MESSAGE_PREFIX = "JW: ";
    public static final String HELPME_MESSAGE = "Help me! I'm in trouble.";
    public static final String THANKS_MESSAGE = "Thank you. I am safe now.";

    // Toast messages ##############################################################################
    public static final String toastSendingHelp = "Calling for HELP....";
    public static final String toastSendingSafe = "Letting them know you're safe.";

    // Special fields ##############################################################################
    public static final String CONTACT_FIELD = "HELP_CONTACT";
    public static final String TRACKING_FIELD = "TRACKING_STATUS";
    public static final String PLAY_SERVICES_FIELD = "PLAY_SERVICES";
    public static final String PREF_FILE = "com.garuda.rama.preferences_1_0";
    public static final String HELP_NUMBER_PREF_KEY = "helpNumber";
    public static final String LOC_REPORTING_PREF_KEY = "sendLocationPref";
    public static final String RAMA_RUNNING_PREF_KEY = "isRamaRunning";


    // Error messages ##############################################################################
    public static final String ERROR_CONTACT_NOT_FOUND = "Emergency contact is either not set or invalid.";
    public static final String ERROR_CANT_SEND_MESSAGE = "Sorry, could not call for help.";
    public static final String LOCATION_SERVICES_404 = "Location services connection failed.";
    public static final String LOCATION_404 = "LOCATION_UNKNOWN";

    public static float REPORTING_DISTANCE_THRESHOLD = 50; // Metres
    public static long REPORTING_TIME_THRESHOLD = (5 * 60 * 1000);     // 5 minutes
    public static final String provider = LocationManager.GPS_PROVIDER;

    // Settings modes ##############################################################################
    public static final int PREFERENCE_MODE = 1;
    public static final int ABOUT_MODE = 2;


    // Prevents instantiation of this class
    private StringConstants() {
    }
}
