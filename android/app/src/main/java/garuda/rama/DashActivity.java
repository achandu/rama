package garuda.rama;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import garuda.rama.location.LocationReporterTask;
import garuda.rama.settings.SettingsActivity;
import garuda.rama.utils.AsyncTaskListener;
import garuda.rama.message.MessagingTask;
import garuda.rama.utils.Genie;
import garuda.rama.utils.ProtocolHandler;
import garuda.rama.utils.StringConstants;


public class DashActivity extends Activity implements AsyncTaskListener<Boolean> {
    private static final String TAG = "DashActivity";
    private Button helpMe;
    private LocationReporterTask locationReporterTask;
    private Context appContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        appContext = getApplicationContext();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash);
        locationReporterTask = new LocationReporterTask(appContext);

        initializeUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_dash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                openSettingsActivity(StringConstants.PREFERENCE_MODE);
                return true;
            case R.id.action_about:
                openSettingsActivity(StringConstants.ABOUT_MODE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initializeUI() {
        helpMe = (Button) findViewById(R.id.helpMe);
        helpMe.setOnClickListener(getHelpMeListener());
        updateHelpMeButton();
    }

    private OnClickListener getHelpMeListener() {
        OnClickListener helpMeListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                Genie.setRamaCallingHelp(!Genie.isRamaCallingHelp(appContext), appContext);
                switchRamaRunner();
            }
        };
        return helpMeListener;
    }

    private void openSettingsActivity(int settingMode) {
        Intent toOpenIntent;
        toOpenIntent = new Intent(appContext, SettingsActivity.class);
        toOpenIntent.putExtra("setting-mode", settingMode);
        this.startActivity(toOpenIntent);
    }

    private void switchRamaRunner() {
        if (Genie.getPhoneNumber(appContext).trim().isEmpty()) {
            Genie.alert(StringConstants.ERROR_CONTACT_NOT_FOUND, appContext);
        } else if (Genie.isRamaCallingHelp(appContext)) {
            sendForHelp();
        } else {
            sendYouAreSafe();
        }
    }

    private void sendForHelp() {
        Genie.setRamaCallingHelp(true, appContext);

        sendMessage(StringConstants.HELPME_MESSAGE);
        sendMessage(ProtocolHandler.generateHelperStart());
        Genie.alert(StringConstants.toastSendingHelp, appContext);
        locationReporterTask.start();
        updateHelpMeButton();
    }

    private void sendYouAreSafe() {
        Genie.setRamaCallingHelp(false, appContext);

        sendMessage(StringConstants.THANKS_MESSAGE);
        sendMessage(ProtocolHandler.generateHelperStop());
        Genie.alert(StringConstants.toastSendingSafe, appContext);
        locationReporterTask.stop();
        updateHelpMeButton();
    }

    private void sendMessage(String toSendMessage) {
        MessagingTask messagingTask = new MessagingTask(DashActivity.this, appContext);
        messagingTask.execute(toSendMessage);
    }


    private void updateHelpMeButton() {
        if (Genie.isRamaCallingHelp(appContext)) {
            helpMe.setText(StringConstants.IAMSAFE);
            helpMe.setBackgroundColor(getResources().getColor(R.color.success_green));
        } else {
            helpMe.setText(StringConstants.HELPME);
            helpMe.setBackgroundColor(getResources().getColor(R.color.warn_red));
        }
    }

    @Override
    public void onTaskComplete(Boolean result) {
    }
}
