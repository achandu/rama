package garuda.rama.settings;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import garuda.rama.R;

/**
 * Created by rchandu on 8/23/14.
 */
public class ShowPreferencesFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
    }
}
