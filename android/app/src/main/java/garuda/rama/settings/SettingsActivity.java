package garuda.rama.settings;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

import garuda.rama.R;
import garuda.rama.utils.StringConstants;


public class SettingsActivity extends Activity {
    private String TAG = "SettingsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_placeholder);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        int settingMode = getIntent().getIntExtra("setting-mode", -1);
        openFragmentByMode(settingMode);
    }

    private void openFragmentByMode(int toOpenMode) {
        int fragmentHolder = R.id.fragmentContent;
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        switch (toOpenMode) {
            case StringConstants.PREFERENCE_MODE:
                fragmentTransaction.replace(fragmentHolder, new ShowPreferencesFragment());
                fragmentTransaction.commit();
                break;
            case StringConstants.ABOUT_MODE:
                fragmentTransaction.replace(fragmentHolder, new AboutFragment());
                fragmentTransaction.commit();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
