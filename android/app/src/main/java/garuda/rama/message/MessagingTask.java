/**
 * Created by rchandu on 8/13/14.
 */
package garuda.rama.message;

import android.content.Context;
import android.os.AsyncTask;

import garuda.rama.utils.AsyncTaskListener;
import garuda.rama.utils.Genie;

/**
 * MessagingTask
 * Behold your worries and place all the messaging tasks inside this guy.
 */
public class MessagingTask extends AsyncTask<String, Void, Boolean> {
    private AsyncTaskListener asyncTaskListener;
    private Context currentContext;
    private String toSendMessage;

    public MessagingTask(AsyncTaskListener taskListener, Context currentContext) {
        this.asyncTaskListener = taskListener;
        this.currentContext = currentContext;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(String... params) {
        this.toSendMessage = params[0];
        return generateHelpMessage(toSendMessage);
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        asyncTaskListener.onTaskComplete(result);
    }

    public boolean generateHelpMessage(String toSendMessage) {
        String phoneNumber = Genie.getPhoneNumber(currentContext);

        if (phoneNumber.isEmpty()) {
            return false;
        }

        Genie.sendSMS(toSendMessage, phoneNumber);
        return true;
    }
}