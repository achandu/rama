package garuda.rama.location;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import garuda.rama.utils.StringConstants;

/**
 * Created by rchandu on 9/19/14.
 */
public abstract class LocationWatcher implements LocationListener {
    String TAG = "LocationReporter";
    private Location latestLocation;
    private Context currentContext;

    Location oldLocation;

    public LocationWatcher(Context currentContext) {
        this.currentContext = currentContext;
    }

    protected void startWatching() {
        LocationManager locationManager = (LocationManager)
                currentContext.getSystemService(Context.LOCATION_SERVICE);

        Criteria requiredCriteria = new Criteria();
        requiredCriteria.setAccuracy(Criteria.ACCURACY_MEDIUM);

        String bestProvider = locationManager.getBestProvider(requiredCriteria, true);
        locationManager.requestLocationUpdates(
                bestProvider, StringConstants.REPORTING_TIME_THRESHOLD,
                StringConstants.REPORTING_DISTANCE_THRESHOLD, this);
        oldLocation = null;
    }

    protected void stopWatching() {
        LocationManager locationManager = (LocationManager)
                currentContext.getSystemService(Context.LOCATION_SERVICE);

        locationManager.removeUpdates(this);
        oldLocation = null;
    }

    @Override
    public void onLocationChanged(Location newLocation) {
        Log.i(TAG, "onLocationChanged");
        latestLocation = newLocation;
        Log.i(TAG, latestLocationAsString());
        oldLocation = newLocation;
        handleLocationChange(newLocation);
    }

    public Location latestLocation() {
        return latestLocation;
    }

    public String latestLocationAsString() {
        return locationAsString(latestLocation);
    }

    public String locationAsString(Location iLocation) {
        if (iLocation == null) {
            return StringConstants.LOCATION_404;
        } else {
            return iLocation.getLatitude() + "," + iLocation.getLongitude();
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderDisabled(String s) {
    }

    @Override
    public void onProviderEnabled(String s) {
    }

    abstract public void handleLocationChange(Location changedLocation);
}
