package garuda.rama.location;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import garuda.rama.message.MessagingTask;
import garuda.rama.utils.AsyncTaskListener;
import garuda.rama.utils.Genie;
import garuda.rama.utils.StringConstants;

/**
 * Created by rchandu on 9/16/14.
 */
public class LocationReporterTask extends LocationWatcher implements AsyncTaskListener<Boolean> {
    String TAG = "LocationReporterTask";
    boolean isStarted;
    Context currentContext;
//--------------------------------------------------------------------------------------------------
// Constructors
//--------------------------------------------------------------------------------------------------

    public LocationReporterTask(Context currentContext) {
        super(currentContext);
        this.currentContext = currentContext;
        isStarted = false;
    }

//--------------------------------------------------------------------------------------------------
// AsyncTaskListener methods
//--------------------------------------------------------------------------------------------------

    public void start() {
        startWatching();
    }

    public void stop() {
        stopWatching();
    }

    public void handleLocationChange(Location changedLocation) {
        Log.i(TAG, currentContext.toString());
        if (Genie.isRamaCallingHelp(currentContext)) {
            Log.i(TAG, "handleLocationChanged");
            MessagingTask messagingHandler = new MessagingTask(LocationReporterTask.this, currentContext);
            messagingHandler.execute(generateMapsLink(changedLocation));
        }
    }

    public void onTaskComplete(Boolean result) {
        if (!result) {
            Genie.alert(StringConstants.ERROR_CANT_SEND_MESSAGE, currentContext);
        } else {
            Log.i(TAG, "Location reported...");
        }
    }

//--------------------------------------------------------------------------------------------------
// Other methods
//--------------------------------------------------------------------------------------------------

    private String generateMapsLink(Location targetLocation) {
        String MAPS_PREFIX = "https://www.google.com/maps/@";
        String mapLinkString = MAPS_PREFIX + targetLocation.getLatitude();
        mapLinkString += "," + targetLocation.getLongitude();
        mapLinkString += ",15z";
        return mapLinkString;
    }
}