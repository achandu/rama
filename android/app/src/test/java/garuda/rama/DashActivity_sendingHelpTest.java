package garuda.rama;

import android.content.Context;
import android.widget.Button;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowLog;
import org.robolectric.shadows.ShadowToast;

import garuda.rama.utils.Genie;
import garuda.rama.utils.StringConstants;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(RobolectricTestRunner.class)
public class DashActivity_sendingHelpTest {
    private DashActivity dashActivity;
    private Button helpMe;
    private Context testContext;

    @Before
    public void setup() {
        ShadowLog.stream = System.out;
        testContext = Robolectric.application.getApplicationContext();
        Genie.setRamaCallingHelp(true,testContext);
        dashActivity = Robolectric.buildActivity(DashActivity.class).create().get();
    }


    @Test
    public void helpMeButton_shouldShouldShowSafeText_whenRamaRunning() {
        Boolean val = Genie.isRamaCallingHelp(Robolectric.application.getApplicationContext());

        helpMe = (Button) dashActivity.findViewById(R.id.helpMe);
        String helpMeText = helpMe.getText().toString();

        assertThat(helpMeText)
                .isEqualTo(StringConstants.IAMSAFE)
                .describedAs("Show " + StringConstants.IAMSAFE + " on help button on launch");
    }

    @Test
    public void clickingHelpMeButton_shouldAlertSendingThanks() {
        String mockDestinationAddress = "9123456789";
        Genie.setPhoneNumber(mockDestinationAddress, testContext);

        helpMe = (Button) dashActivity.findViewById(R.id.helpMe);
        helpMe.performClick();

        assertThat(ShadowToast.getTextOfLatestToast())
                .isEqualTo(StringConstants.toastSendingSafe)
                .describedAs("Should show notification telling them you're safe.");
    }

    //@Test
    public void clickingHelpMeButton_shouldSetRamaAsRunningAndUpdateUI() {
        String mockDestinationAddress = "9123456789";
        Genie.setPhoneNumber(mockDestinationAddress, testContext);

        helpMe = (Button) dashActivity.findViewById(R.id.helpMe);
        helpMe.performClick();

        assertThat(Genie.isRamaCallingHelp(testContext))
                .isEqualTo(true)
                .describedAs("Should set rama running as true.");

        assertThat(helpMe.getText())
                .isEqualTo(StringConstants.IAMSAFE)
                .describedAs("Should update the text.");
    }
}
