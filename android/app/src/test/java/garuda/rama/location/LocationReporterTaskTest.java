package garuda.rama.location;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.telephony.SmsManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowLocationManager;
import org.robolectric.shadows.ShadowSmsManager;

import garuda.rama.utils.Genie;
import garuda.rama.utils.StringConstants;

import static org.assertj.core.api.Assertions.assertThat;
import static org.robolectric.Robolectric.shadowOf;

@RunWith(RobolectricTestRunner.class)
public class LocationReporterTaskTest {
    private LocationReporterTask locationReporterTask;
    private String mockDestinationAddress = "9123456789";
    private Context test_context;

    private double mockLatitude = 42.474022, mockLongitude = -83.415698;
    private double mockLatitude2 = 42.474332, mockLongitude2 = -83.415715;
    private double mockLatitude3 = 42.476567, mockLongitude3 = -83.417180;
    private static final String MAPS_PREFIX = "https://www.google.com/maps/@";

    private Location getCurrentLocation(LocationManager locationManager, double latitude, double longitude) {
        String bestProvider = locationManager.getBestProvider(new Criteria(), true);

        Location location = new Location(bestProvider);
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        location.setTime(System.currentTimeMillis());
        return location;
    }

    private String getLocationAsString(Location iLocation) {
        if (iLocation == null) {
            return StringConstants.LOCATION_404;
        } else {
            return iLocation.getLatitude() + "," + iLocation.getLongitude();
        }
    }

    private String generateMapsLocationLink(double latitude, double longitude) {
        return MAPS_PREFIX + latitude + "," + longitude + ",15z";
    }

    @Before
    public void setUp() {
        StringConstants.REPORTING_TIME_THRESHOLD = 0;
        test_context = Robolectric.application.getApplicationContext();

        Genie.setPhoneNumber(mockDestinationAddress, test_context);
        locationReporterTask = new LocationReporterTask(test_context);
        locationReporterTask.start();
    }

    @After
    public void tearDown() {
        locationReporterTask.stop();
    }

    @Test
    public void getLatestLocation_shouldReturnError_whenLocationIsNotFound() {
        assertThat(locationReporterTask.latestLocationAsString())
                .isEqualTo(StringConstants.LOCATION_404)
                .describedAs("Give error when location cannot be found");
    }

    @Test
    public void getLatestLocation_shouldReturnLatestLocation() {
        LocationManager locationManager = (LocationManager)
                Robolectric.application.getSystemService(Context.LOCATION_SERVICE);
        ShadowLocationManager shadowLocationManager = shadowOf(locationManager);
        Location expectedLocation = getCurrentLocation(locationManager, 12.0, 20.0);

        shadowLocationManager.simulateLocation(expectedLocation);
        Location actualLocation = locationReporterTask.latestLocation();

        assertThat(actualLocation).isEqualTo(expectedLocation);
    }

    @Test
    public void locationChange_shouldNotSendLocation_whenLocationChangeIsLessThanThreshold() {
        LocationManager locationManager = (LocationManager)
                Robolectric.application.getSystemService(Context.LOCATION_SERVICE);
        Location expectedLocation1 = getCurrentLocation(locationManager, mockLatitude, mockLongitude);
        Location expectedLocation2 = getCurrentLocation(locationManager, mockLatitude2, mockLongitude2);

        ShadowLocationManager shadowLocationManager = shadowOf(locationManager);
        ShadowSmsManager shadowSmsManager = shadowOf(SmsManager.getDefault());

        Genie.setRamaCallingHelp(true, test_context);

        shadowLocationManager.simulateLocation(expectedLocation1);
        shadowLocationManager.simulateLocation(expectedLocation2);

        Robolectric.runUiThreadTasksIncludingDelayedTasks();
        Robolectric.runBackgroundTasks();
        ShadowSmsManager.TextSmsParams lastSentTextMessageParams = shadowSmsManager.getLastSentTextMessageParams();

        assertThat(lastSentTextMessageParams.getText())
                .isNotEqualTo(getLocationAsString(expectedLocation2))
                .describedAs("Sent current location");
    }


/*    @Test
    public void locationChange_shouldSendLocation_whenStarted() {
        LocationManager locationManager = (LocationManager)
                Robolectric.application.getSystemService(Context.LOCATION_SERVICE);
        Location expectedLocation2 = getCurrentLocation(locationManager, mockLatitude3, mockLongitude3);

        ShadowLocationManager shadowLocationManager = shadowOf(locationManager);
        ShadowSmsManager shadowSmsManager = shadowOf(SmsManager.getDefault());

        Genie.setRamaCallingHelp(true, test_context);

        shadowLocationManager.simulateLocation(expectedLocation2);

        Robolectric.runUiThreadTasksIncludingDelayedTasks();
        Robolectric.runBackgroundTasks();
        ShadowSmsManager.TextSmsParams lastSentTextMessageParams = shadowSmsManager.getLastSentTextMessageParams();

        assertThat(lastSentTextMessageParams.getDestinationAddress())
                .isEqualTo(mockDestinationAddress)
                .describedAs("Message was sent to correct address");

        assertThat(lastSentTextMessageParams.getText())
                .isEqualTo(getLocationAsString(expectedLocation2))
                .describedAs("Sent current location");
    }*/

    @Test
    public void locationChange_shouldGoogleMapsLink_whenLocationChanged() {
        LocationManager locationManager = (LocationManager)
                Robolectric.application.getSystemService(Context.LOCATION_SERVICE);
        Location expectedLocation2 = getCurrentLocation(locationManager, mockLatitude3, mockLongitude3);

        ShadowLocationManager shadowLocationManager = shadowOf(locationManager);
        ShadowSmsManager shadowSmsManager = shadowOf(SmsManager.getDefault());

        Genie.setRamaCallingHelp(true, test_context);

        shadowLocationManager.simulateLocation(expectedLocation2);

        Robolectric.runUiThreadTasksIncludingDelayedTasks();
        Robolectric.runBackgroundTasks();
        ShadowSmsManager.TextSmsParams lastSentTextMessageParams = shadowSmsManager.getLastSentTextMessageParams();

        assertThat(lastSentTextMessageParams.getDestinationAddress())
                .isEqualTo(mockDestinationAddress)
                .describedAs("Message was sent to correct address");

        assertThat(lastSentTextMessageParams.getText())
                .isEqualTo(generateMapsLocationLink(expectedLocation2.getLatitude(), expectedLocation2.getLongitude()))
                .describedAs("Sent current location");
    }

}
