package garuda.rama;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

import org.robolectric.Robolectric;

import garuda.rama.settings.SettingsActivity;

/**
 * Created by rchandu on 8/30/14.
 */
public class TestHelpers {

    public static void startFragment(Fragment fragment) {
        Activity currentActivity = Robolectric.buildActivity(SettingsActivity.class).create().get();
        FragmentManager fragmentManager = currentActivity.getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(fragment, null);
        fragmentTransaction.commit();
    }
}
