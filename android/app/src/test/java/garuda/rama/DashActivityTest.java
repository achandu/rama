package garuda.rama;

import android.content.Context;
import android.content.Intent;
import android.view.MenuItem;
import android.widget.Button;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowLog;
import org.robolectric.shadows.ShadowToast;
import org.robolectric.tester.android.view.TestMenuItem;

import garuda.rama.settings.SettingsActivity;
import garuda.rama.utils.Genie;
import garuda.rama.utils.StringConstants;

import static org.assertj.core.api.Assertions.assertThat;
import static org.robolectric.Robolectric.shadowOf;


@RunWith(RobolectricTestRunner.class)
public class DashActivityTest {
    private DashActivity dashActivity;
    private Button helpMe;
    private Context testContext;

    @Before
    public void setup() {
        ShadowLog.stream = System.out;
        testContext = Robolectric.application.getApplicationContext();
        Genie.setRamaCallingHelp(false, testContext);
        dashActivity = Robolectric.buildActivity(DashActivity.class).create().get();
    }

    @Test
    public void activity_shouldNotBeNull() throws Exception {
        assertThat(dashActivity).isNotNull();
    }

    // Help me button ##############################################################################
    @Test
    public void launch_shouldDisplayHelpMessage() throws Exception {
        helpMe = (Button) dashActivity.findViewById(R.id.helpMe);

        assertThat(helpMe).isNotNull().describedAs("Expected Dashboard Activity to not be null");
        assertThat(helpMe.getText().toString())
                .isEqualTo(StringConstants.HELPME)
                .describedAs("Expected button on dashboard to show " + StringConstants.HELPME);
    }

    @Test
    public void helpMeButton_shouldShouldShowHelpText_whenRamaNotRunning() {
        helpMe = (Button) dashActivity.findViewById(R.id.helpMe);
        String helpMeText = helpMe.getText().toString();

        assertThat(helpMeText)
                .isEqualTo(StringConstants.HELPME)
                .describedAs("Show " + StringConstants.HELPME + " on help button on launch");
    }

    @Test
    public void clickingHelpMeButton_shouldAlertSendingHelp() {
        String mockDestinationAddress = "9123456789";
        Genie.setPhoneNumber(mockDestinationAddress, testContext);

        helpMe = (Button) dashActivity.findViewById(R.id.helpMe);
        helpMe.performClick();

        assertThat(ShadowToast.getTextOfLatestToast())
                .isEqualTo(StringConstants.toastSendingHelp)
                .describedAs("Should show notification that message was sent");
    }

    @Test
    public void clickingHelpMeButton_shouldSetRamaAsRunningAndUpdateUI() {
        String mockDestinationAddress = "9123456789";
        Genie.setPhoneNumber(mockDestinationAddress, testContext);

        helpMe = (Button) dashActivity.findViewById(R.id.helpMe);
        helpMe.performClick();

        assertThat(Genie.isRamaCallingHelp(testContext))
                .isEqualTo(true)
                .describedAs("Should set rama running as true.");

        assertThat(helpMe.getText())
                .isEqualTo(StringConstants.IAMSAFE)
                .describedAs("Should update the text.");
    }

    @Test
    public void clickingHelpMeButton_whenHelpContactNotSet_shouldAlertContactNotFound() {
        Genie.setPhoneNumber("", testContext);
        helpMe = (Button) dashActivity.findViewById(R.id.helpMe);
        helpMe.performClick();

        assertThat(ShadowToast.getTextOfLatestToast())
                .isEqualTo(StringConstants.ERROR_CONTACT_NOT_FOUND)
                .describedAs("Should show notification that message was sent");
    }

    // Settings - preferences ######################################################################
    @Test
    public void clickingSettings_shouldOpenSettingsWithPreferenceMode() {
        ShadowActivity shadowActivity = shadowOf(dashActivity);

        MenuItem item = new TestMenuItem(R.id.action_settings);
        dashActivity.onOptionsItemSelected(item);

        Intent startedIntent = shadowActivity.getNextStartedActivity();

        assertThat(startedIntent.getComponent().getClassName())
                .isEqualTo(SettingsActivity.class.getName())
                .describedAs("Should start the settings activity");

        int settingMode = startedIntent.getIntExtra("setting-mode", -1);
        assertThat(settingMode).isNotNull();
        assertThat(settingMode).isEqualTo(StringConstants.PREFERENCE_MODE);
    }

    // Settings - About us #########################################################################
    @Test
    public void clickingSettings_shouldOpenSettingsWithAboutMode() {
        ShadowActivity shadowActivity = shadowOf(dashActivity);

        MenuItem item = new TestMenuItem(R.id.action_about);
        dashActivity.onOptionsItemSelected(item);

        Intent startedIntent = shadowActivity.getNextStartedActivity();

        assertThat(startedIntent.getComponent().getClassName())
                .isEqualTo(SettingsActivity.class.getName())
                .describedAs("Should start the settings activity");

        int settingMode = startedIntent.getIntExtra("setting-mode", -1);
        assertThat(settingMode).isNotNull();
        assertThat(settingMode).isEqualTo(StringConstants.ABOUT_MODE);

    }
}