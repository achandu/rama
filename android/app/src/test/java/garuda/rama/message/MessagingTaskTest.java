package garuda.rama.message;

import android.telephony.SmsManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowSmsManager;

import garuda.rama.utils.AsyncTaskListener;
import garuda.rama.utils.Genie;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.robolectric.Robolectric.shadowOf;

@RunWith(RobolectricTestRunner.class)
public class MessagingTaskTest {
    private MockClass mockClass;

    private class MockClass implements AsyncTaskListener<Boolean> {
        @Override
        public void onTaskComplete(Boolean result) {
        }
    }

    @Before
    public void setup() {
        mockClass = new MockClass();
        Robolectric.getBackgroundScheduler().pause();
        Robolectric.getUiThreadScheduler().pause();
    }

    @Test
    public void shouldExpectedSmsToHelpNumber_whenTaskIsExecuted() {
        MessagingTask messagingTask = new MessagingTask(mockClass, Robolectric.application);

        String mockDestinationAddress = "9123456789";
        Genie.setPhoneNumber(mockDestinationAddress, Robolectric.application.getApplicationContext());
        String mockText = "test-data";

        messagingTask.execute(mockText);
        Robolectric.runBackgroundTasks();
        Robolectric.runUiThreadTasks();

        ShadowSmsManager shadowSmsManager = shadowOf(SmsManager.getDefault());
        ShadowSmsManager.TextSmsParams lastSentTextMessageParams = shadowSmsManager.getLastSentTextMessageParams();

        assertThat(
                "Message sent to correct address",
                lastSentTextMessageParams.getDestinationAddress(),
                equalTo(mockDestinationAddress)
        );

        assertThat(
                "Verify correct message has been sent",
                lastSentTextMessageParams.getText(),
                equalTo(mockText)
        );
    }
}