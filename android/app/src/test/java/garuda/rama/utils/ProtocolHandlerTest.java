package garuda.rama.utils;

import android.location.Location;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import garuda.rama.utils.ProtocolHandler;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by rchandu on 11/4/14.
 */
@RunWith(RobolectricTestRunner.class)
public class ProtocolHandlerTest {

    public static Location createLocation() {
        final String PROVIDER = "flp";
        final double LAT = 37.377166;
        final double LONG = -122.086966;
        final float ACCURACY = 3.0f;

        Location newLocation = new Location(PROVIDER);
        newLocation.setLatitude(LAT);
        newLocation.setLongitude(LONG);
        newLocation.setAccuracy(ACCURACY);
        return newLocation;
    }


    @Test
    public void isHelperStart_shouldReturnFalseForInvalidInput() {
        String[] mockInvalidHelperStart = {"invalidData", "rh:stop", "r:stop", "12rh:start"};
        for (int i = 0; i < mockInvalidHelperStart.length; i++) {
            assertFalse(ProtocolHandler.isHelperStart(mockInvalidHelperStart[i]));
        }
    }

    @Test
    public void isHelperStart_shouldReturnTrueForValidInput() {
        String[] mockValidHelperStart = {"rh:start", "rh:start HELP!!!"};
        for (int i = 0; i < mockValidHelperStart.length; i++) {
            assertTrue(ProtocolHandler.isHelperStart(mockValidHelperStart[i]));
        }
    }

    @Test
    public void isHelperStop_shouldReturnFalseForInvalidInput() {
        String[] mockInvalidHelperStop = {"invalidData", "rh:start", "rloc:", "r:stop", "rh:stap", "12rh:stop"};
        for (int i = 0; i < mockInvalidHelperStop.length; i++) {
            assertFalse(ProtocolHandler.isHelperStop(mockInvalidHelperStop[i]));
        }
    }

    @Test
    public void isHelperStop_shouldReturnTrueForValidInput() {
        String[] mockValidHelperStop = {"rh:stop", "rh:stop Works"};
        for (int i = 0; i < mockValidHelperStop.length; i++) {
            assertTrue(ProtocolHandler.isHelperStop(mockValidHelperStop[i]));
        }
    }

    @Test
    public void isLocationReporter_shouldReturnFalseForInvalidInput() {
        String[] mockInvalidLocationReporter = {
                "invalidData", "rh:start", "rloc:", "r:stop",
                "rloc:=42.4745292,-83.4155876", "rloc:42.4745292",
                "rloc:-83.4155876", "rloc42.4745292,-83.4155876",
                "rloc:42.4745292-83.4155876"
        };

        for (int i = 0; i < mockInvalidLocationReporter.length; i++) {
            assertFalse(ProtocolHandler.isLocationReporter(mockInvalidLocationReporter[i]));
        }
    }

    @Test
    public void isLocationReporter_shouldReturnTrueForValidInput() {
        String[] mockValidLocationReporter = {
                "rloc:42.4745292,-83.4155876", "rloc:42.4745292,+83.4155876",
                "rloc:+42.4745292,-83.4155876", "rloc:+42.4745292,+83.4155876",
                "rloc:-42.4745292,-83.4155876", "rloc:-42.4745292,+83.4155876",
                "rloc:-42,-83", "rloc:-42,+83.4155876"
        };

        for (int i = 0; i < mockValidLocationReporter.length; i++) {
            assertTrue(ProtocolHandler.isLocationReporter(mockValidLocationReporter[i]));
        }
    }

    @Test
    public void generateHelperStart_shouldReturnValidHelperStartString() {
        String generatedHelperStart = ProtocolHandler.generateHelperStart();
        assertTrue(ProtocolHandler.isHelperStart(generatedHelperStart));
    }

    @Test
    public void generateHelperStart_withInput_shouldReturnValidHelperStartStringAndAppendInput() {
        String generatedHelperStart = ProtocolHandler.generateHelperStart("This ain't gonna work!");
        assertTrue(ProtocolHandler.isHelperStart(generatedHelperStart));
    }

    @Test
    public void generateHelperStop_shouldReturnValidHelperStopString() {
        String generatedHelperStart = ProtocolHandler.generateHelperStop();
        assertTrue(ProtocolHandler.isHelperStop(generatedHelperStart));
    }

    @Test
    public void generateHelperStop_withInput_shouldReturnValidHelperStopStringAndAppendInput() {
        String generatedHelperStart = ProtocolHandler.generateHelperStop("This ain't gonna work!");
        assertTrue(ProtocolHandler.isHelperStop(generatedHelperStart));
    }

    @Test
    public void generateLocationReporter_shouldReturnValidLocationReporterString() {
        Location mockLocation = createLocation();
        String generatedHelperStart = ProtocolHandler.generateLocationReporter(mockLocation);
        assertTrue(ProtocolHandler.isLocationReporter(generatedHelperStart));
    }
}
