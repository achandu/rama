package garuda.rama.utils;

import android.content.Context;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowToast;

import garuda.rama.utils.Genie;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
public class GenieTest {
    private String testToastText = "Test toast";

    @Before
    public void setup() {
        Genie.reset();
    }

    // Testing getAppContext() and setAppContext() #################################################
    @Test(expected = NullPointerException.class)
    public void getAppContext_whenContextNotSet_shouldThrowNullPointerException() throws Exception {
        Genie.getAppContext();
    }

    @Test
    public void setAppContext_shouldSetContext() {
        Genie.setAppContext(Robolectric.application);
        assertThat(
                "Expected setAppContext to store the AppContext provided.",
                Genie.getAppContext(),
                equalTo(Robolectric.application.getApplicationContext()));
    }


    // Testing alert() #############################################################################
    @Test(expected = NullPointerException.class)
    public void alertWithoutContextParameter_whenContextNotSet_shouldThrowNullPointerException() {
        Genie.reset();
        Genie.alert(this.testToastText);
    }

    @Test
    public void alertWithoutContextParameter_shouldShowToast() {
        Genie.setAppContext(Robolectric.application);
        Genie.alert(this.testToastText);
        assertThat(
                "Expected to show " + this.testToastText,
                ShadowToast.getTextOfLatestToast(),
                equalTo(this.testToastText));
    }

    @Test
    public void alertWithContextParameter_shouldShowToast() {
        Genie.alert(this.testToastText, Robolectric.application);
        assertThat(
                "Expected to show " + this.testToastText,
                ShadowToast.getTextOfLatestToast(),
                equalTo(this.testToastText));
    }

    // Testing getPhoneNumber() and setPhoneNumber() ###############################################
    @Test
    public void getPhoneNumber_whenHelpNumberNotSet_shouldReturnEmpty() {
        assertThat(
                "getPhoneNumber() without settting should show Toast that it failed.",
                Genie.getPhoneNumber(Robolectric.application.getApplicationContext()),
                equalTo(""));
    }

    @Test
    public void setPhoneNumber_shouldSetPhoneNumberToPreferences() {
        String tempPhoneNumber = "tempSetNow";
        Genie.setPhoneNumber(tempPhoneNumber, Robolectric.application.getApplicationContext());

        assertThat(
                "SetPhoneNumber should set the Help Number into Preferences",
                Genie.getPhoneNumber(Robolectric.application.getApplicationContext()),
                equalTo(tempPhoneNumber));
    }

    // Testing isLocationReportingEnabled() ########################################################
    @Test
    public void locationReporting_shouldReturnTrue_whenPreferenceNotSet() {
        assertTrue("isLocationReportingEnabled() without settting should return true.",
                Genie.isLocationReportingEnabled(Robolectric.application.getApplicationContext()));
    }

    @Test
    public void locationReporting_shouldReturnPreference_whenSet() {
        Context robolectricContext = Robolectric.application.getApplicationContext();

        Genie.setLocationReportingEnabled(false, Robolectric.application.getApplicationContext());
        assertFalse("isLocationReportingEnabled() when set false should return false.",
                Genie.isLocationReportingEnabled(robolectricContext));

        Genie.setLocationReportingEnabled(true, Robolectric.application.getApplicationContext());
        assertTrue("isLocationReportingEnabled() when set true should return true.",
                Genie.isLocationReportingEnabled(robolectricContext));
    }

    // Testing isRamaCallingHelp() #################################################################
    @Test
    public void isRamaCallingHelp_shouldReturnPreference_whenSet() {
        Context robolectricContext = Robolectric.application.getApplicationContext();

        Genie.setRamaCallingHelp(false, Robolectric.application.getApplicationContext());
        assertFalse("isLocationReportingEnabled() when set false should return false.",
                Genie.isRamaCallingHelp(robolectricContext));

        Genie.setRamaCallingHelp(true, Robolectric.application.getApplicationContext());
        assertTrue("isLocationReportingEnabled() when set true should return true.",
                Genie.isRamaCallingHelp(robolectricContext));
    }
}
