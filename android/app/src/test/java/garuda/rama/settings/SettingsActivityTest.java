package garuda.rama.settings;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.widget.FrameLayout;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import garuda.rama.R;
import garuda.rama.utils.StringConstants;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(RobolectricTestRunner.class)
public class SettingsActivityTest {
    private FrameLayout settingsPlaceHolder;

    @Test
    public void activity_shouldNotBeNull() {
        SettingsActivity settingsActivity = Robolectric.buildActivity(SettingsActivity.class).create().get();
        assertThat(settingsActivity).isNotNull();

        settingsPlaceHolder = (FrameLayout) settingsActivity.findViewById(R.id.fragmentContent);
        assertThat(settingsPlaceHolder).isNotNull();
    }

    @Test
    public void settingsActivityWithAboutMode_shouldAttachAboutFragment() {
        Intent intent = new Intent();
        intent.putExtra("setting-mode", StringConstants.ABOUT_MODE);
        SettingsActivity settingsActivity = Robolectric.buildActivity(SettingsActivity.class).withIntent(intent).create().get();

        FragmentManager fragmentManager = settingsActivity.getFragmentManager();
        assertThat(fragmentManager).isNotNull();

        Fragment toAssertFragment = fragmentManager.findFragmentById(R.id.fragmentContent);
        assertThat(toAssertFragment).isNotNull();

        String toAssertFragmentName = toAssertFragment.getClass().getCanonicalName();
        assertThat(toAssertFragmentName).isEqualTo(AboutFragment.class.getCanonicalName());
    }

    @Test
    public void settingsActivityWithPreferencesMode_shouldAttachAboutFragment() {
        Intent intent = new Intent();
        intent.putExtra("setting-mode", StringConstants.PREFERENCE_MODE);
        SettingsActivity settingsActivity = Robolectric.buildActivity(SettingsActivity.class).withIntent(intent).create().get();

        FragmentManager fragmentManager = settingsActivity.getFragmentManager();
        assertThat(fragmentManager).isNotNull();

        Fragment toAssertFragment = fragmentManager.findFragmentById(R.id.fragmentContent);
        assertThat(toAssertFragment).isNotNull();

        String toAssertFragmentName = toAssertFragment.getClass().getCanonicalName();
        assertThat(toAssertFragmentName).isEqualTo(ShowPreferencesFragment.class.getCanonicalName());
    }
}
