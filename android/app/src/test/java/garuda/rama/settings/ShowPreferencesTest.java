package garuda.rama.settings;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import garuda.rama.TestHelpers;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(RobolectricTestRunner.class)
public class ShowPreferencesTest {

    @Test
    public void shouldNotBeNull() throws Exception {
        ShowPreferencesFragment showPreferencesFragment = new ShowPreferencesFragment();
        TestHelpers.startFragment(showPreferencesFragment);
        assertThat(showPreferencesFragment).isNotNull();
    }
}
