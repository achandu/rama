package garuda.rama.settings;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import garuda.rama.TestHelpers;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(RobolectricTestRunner.class)
public class AboutFragmentTest {

    @Test
    public void shouldNotBeNull() throws Exception {
        AboutFragment aboutFragment = new AboutFragment();
        TestHelpers.startFragment(aboutFragment);
        assertThat(aboutFragment).isNotNull();
    }
}
